//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class GPC_CriterioEvaluacion
    {
        public GPC_CriterioEvaluacion()
        {
            this.GPC_DetalleEvaluacion = new HashSet<GPC_DetalleEvaluacion>();
        }
    
        public int Codigo_Criterio { get; set; }
        public string Nombre_Criterio { get; set; }
        public double Rango_Inicial { get; set; }
        public double Rango_Final { get; set; }
        public string Comentario { get; set; }
        public string UsuarioCreacion { get; set; }
        public Nullable<System.DateTime> FechaHoraCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public Nullable<System.DateTime> FechaHoraActualizacion { get; set; }
        public int Estado_Criterio { get; set; }
    
        public virtual ICollection<GPC_DetalleEvaluacion> GPC_DetalleEvaluacion { get; set; }
    }
}
